TARGETNAME 	= Minium

# Main Compiler
CC = g++ 
CFLAGS = -g -std=c++11
SRCEXT = cpp

# Directories
BUILDDIR = build

TARGET = $(TARGETNAME)

$(TARGET): main.cpp
	 $(CC) $(CFLAGS) $< -o $(TARGETNAME)

.PHONY: clean
clean:
	rm -f $(TARGETNAME).exe
