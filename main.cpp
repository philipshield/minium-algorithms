

#include <iostream>
#include <cstdio>
#include <string>
#include <vector>

#include "minium/minium.hpp"

void commonTests() 
{
	std::cout << "======== Common Tests ========" << std::endl;
	std::cout << "sgn(10) = " << minium::sgn(10) <<std::endl;
	std::cout << "sgn(-6) = " << minium::sgn(-6) <<std::endl;
	std::cout << "sgn(0) = " << minium::sgn(0) <<std::endl;
}

void fenwickTests() 
{
	std::cout << "======== Fenwick Tree Tests ========" << std::endl;
	minium::FenwickTree<int> fw(10);

	std::cout << " - update(4,5)" << std::endl;
	fw.update(4, 5);

	std::cout << " - update(2,6)" << std::endl;
	fw.update(2, 6);

	for (int i = 0; i < 10; ++i)
		std::cout << " - query(" << i << ") -> " << fw.query(i) << std::endl;

}

void modularTests() 
{
	std::cout << "======== Modular Tests ========" << std::endl;

	std::cout << "44 mod 7 = " << minium::mod(44, 7) << std::endl;
	std::cout << "gcd(123, 315) = " << minium::gcd(123, 315) << std::endl;
	std::cout << "lcm(3, 7) = " << minium::lcm(3, 7) << std::endl;
	std::cout << "integer division 55/3 = " << minium::int_div(55, 3) << std::endl;

	std::pair<int, int> xy = minium::euclidean(240, 46);
	std::cout << "euclidean(240, 46) = (" << xy.first << "," << xy.second << ")" << std::endl;

	std::cout << " 5+6 mod 7 = " << minium::mod_add(5, 6, 7) << std::endl;
	std::cout << " 5-6 mod 7 = " << minium::mod_sub(5, 6, 7) << std::endl;
	std::cout << " 5*6 mod 23 = " << minium::mod_mult(5, 6, 23) << std::endl; 
	std::cout << " 22/2 mod 7 = " << minium::mod_div(22, 2, 7) << std::endl; 
	std::cout << " 3^4 mod 73 = " << minium::mod_pow(3, 4, 73) << std::endl;
	
}

void rationalTests() 
{
	std::cout << "======== Rational Tests ========" << std::endl;

	minium::Rational<int> r1(1234, 54);
	std::cout << "1234/54 => " << r1 << std::endl;

	std::cout << "1/2 => " << minium::Rational<int>(1, 2) << std::endl;
	std::cout << "2/4 => " << minium::Rational<int>(2, 4) << std::endl;
	std::cout << "2/4 not reduced => " <<minium::Rational<int>(2, 4, false) << std::endl;

	minium::Rational<int> r2(2, 3);
	minium::Rational<int> r3 = r2;
	std::cout << "assng 2/3 => " << r3 << std::endl;

}

void suffixArrayTests()
{
	std::cout << "======== Suffix Array Tests ========" << std::endl;
	std::string S = "hejhopphejja";
	minium::SuffixArray sa(S);

	//TODO:
}

void UnionFindTests()
{
	std::cout << "======== Union Find Tests ========" << std::endl;
	minium::UnionFind sets(10);

	std::cout << "1 and 5 in same => " << (sets.same(1,5) ? "true" : "false") << std::endl;
	std::cout << "merge 1 and 5" << std::endl;
	sets.merge(1, 5);
	std::cout << "1 and 5 in same => " << (sets.same(1,5) ? "true" : "false") << std::endl;
}

void LisTests()
{
	std::cout << "======== LIS Tests ========" << std::endl;
	std::vector<int> seq = {5,19,5,81,50,28,29,1,83,23};

	std::cout << "seq:	";
	for(int i : seq) std::cout << i << " ";
	std::cout << std::endl;

	std::vector<int> lis = minium::lis(seq);
	std::cout << "lis:	";
	for(int i : lis) std::cout << i << " ";
	std::cout << std::endl;

	for(int i : lis) std::cout << seq[i] << " ";
	std::cout << std::endl;

}

void intervalCoverTests()
{
	std::cout << "======== Interval Cover Tests ========" << std::endl;
	
	int N;
    double start, end;
    std::vector<int> res;
    while(scanf("%lf%lf", &start, &end) == 2) {
    	std::cout << "interval: " << start << " -- " << end << std::endl;
        minium::Interval interval(start,end);
        scanf("%d", &N);
        std::vector<minium::Interval> intervals(N);
        for (int i = 0; i < N; ++i) {
            scanf("%lf%lf", &start, &end);
            intervals[i].start = start;
            intervals[i].end = end;
            intervals[i].index = i;
        }
        res = minium::cover(interval, intervals);
		if (!res.empty()) {
            std::cout << res.size() << "\n";
			for (int i = 0; i < res.size(); ++i) std::cout << res[i] << " ";
        }
		else
			std::cout << "impossible";
        std::cout << std::endl;
    }
}

void graphTests() 
{
	std::cout << "======== Graph Tests ========" << std::endl;

	std::cout << "creating graph with 10 nodes" << std::endl;
	minium::Graph graph(10);

	graph.addEdge(0, 3, 5);
	graph.addEdge(0, 6, 1);
	minium::Node n0 = graph.getNode(0);
	std::cout << "n0: " << n0.id << std::endl;

	for(const minium::Edge& edge : n0.adj) 
	{
		std::cout << "n0 --(" << edge.w << ")--> " << graph.getNode(edge.to).id << std::endl;
	}
}

void MSTTests()
{
	std::cout << "======== MST Tests ========" << std::endl;

	minium::Graph G(6);
	G.addEdge(0, 1);
	G.addEdge(1, 3);
	G.addEdge(1, 4);
	G.addEdge(3, 2);
	G.addEdge(2, 4);
	G.addEdge(0, 2);
	G.addEdge(2, 5);

	std::vector<minium::Edge> mst = minium::MinimumSpanningTree(G);
	for(const minium::Edge& e : mst) 
	{
		std::cout << e.from << " -- " << e.to << std::endl;
	}

}

int main() 
{
	//commonTests();
	//fenwickTests();
	//modularTests();
	//rationalTests();
	//suffixArrayTests();
	//UnionFindTests();
	//LisTests();
	//intervalCoverTests();
	//graphTests();
	MSTTests();

	return 0;
}