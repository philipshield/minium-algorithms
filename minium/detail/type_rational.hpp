///////////////////////////////////////////////////////////////////////////////////
/// Minium Algorithm Library.
///
///		Rational Number Class
///
/// A class that handles rational numbers of the form (x/y)
/// 
/// Supported operations:
/// +, -, *, /, reduction, comparison, printing (ostream)
///
///
///////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "func_modular.hpp"

namespace minium 
{

const bool SHOW_DIV_BY_1 = true;

template<typename T>
class Rational;

template<typename T>
std::ostream& operator<<(std::ostream&, const Rational<T>&); // make printable with cout

template<typename T>
class Rational
{
public: 
	Rational(T numerator, T denominator, bool doreduce = true);
	Rational(T numerator);
	Rational();

	friend std::ostream& operator<< <>(std::ostream& o, const Rational<T>& r); // printable with cout

	Rational inverse() const;

	Rational& operator=(const Rational& other);
	Rational operator*(const Rational& other) const;
	Rational operator+(const Rational& other) const;
	Rational operator-(const Rational& other) const;
	Rational operator/(const Rational& other) const;
	const bool operator==(const Rational& other) const;
	const bool operator!=(const Rational& other) const;
	const bool operator<=(const Rational& other) const;
	const bool operator>=(const Rational& other) const;
	const bool operator<(const Rational& other) const;
	const bool operator>(const Rational& other) const;
	
protected:
private:
	bool reduced;
	T numerator;
	T denominator;

	void reduce()
	{
		T d = gcd(numerator, denominator);
		numerator /= d;
		denominator /= d;

		if (sgn(denominator) == -1)
		{
			numerator *= -1;
			denominator *= -1;
		}

		reduced = true;
	}
   
};


}//namespace minium

#ifndef MINIUM_EXTERNAL_TEMPLATE
#include "type_rational.inl"
#endif//MINIUM_EXTERNAL_TEMPLATE