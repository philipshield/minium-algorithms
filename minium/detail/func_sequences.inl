///////////////////////////////////////////////////////////////////////////////////
/// Minium Algorithm Library.
///
/// 	Sequence Algorithms
///
///////////////////////////////////////////////////////////////////////////////////

#include <algorithm>
#include <functional>

namespace minium
{

std::vector<int> lis(std::vector<int> objects)
{
	auto binsearch_lis = [&](std::vector<int>& objects, std::vector<int>& S, int L, int i)->int
	{
		int low = 0, mid = -1, high = L-1, best = -1;
	    while (high >= low) 
	    {
	        mid = low + (high-low)/2;
	        if (objects[S[mid]] == objects[i]) return -1;
	        if (objects[S[mid]] < objects[i]) 
	        {
	            low = mid + 1;
	        }
	        else 
	        {
	            best = mid;
	            high = mid - 1;
	        }
	    }
	    return best;
	};

	if (objects.empty()) 
		return std::vector<int>();

    std::vector<int> res;
    int N = objects.size();
    std::vector<int> S(N);       // S[j] = position j <= L of smallest object that a LIS of length j end on
    std::vector<int> prev(N);    // prev[k] index of successor to objects[k]
    
    int L = 1; // longest LIS found so far
    S[0] = 0;
    prev[0] = -1;

    // iteratively build S and prev (O(nlog(n)))
    for(int i = 1; i < N; ++i)
    {
        if (objects[i] > objects[S[L-1]]) 
        {
            S[L] = i;
            prev[i] = S[L-1];
            ++L;
        }
        else 
        {
            int j = binsearch_lis(objects, S, L, i); // objects[S[j]] >= objects[i]
            if (j != -1) 
            {
                prev[i] = S[j-1];
                S[j] = i;
            }
        }
    }

    // backtrack to find the LIS
    int current = S[L-1];
    res.resize(L);
    for(int i = L-1; i >= 0; --i)
    {
        res[i] = current; 
        current = prev[current];
    }
    return res;
}

std::vector<int> cover(Interval interval, std::vector<Interval> intervals) 
{
    double start, end;
    std::vector<int> cover;
    bool zeroInterval = interval.start == interval.end;

    std::sort(intervals.begin(), intervals.end()); // sorted by end

    double covered = interval.start;
    while (covered < interval.end || zeroInterval) 
    {
        int foundIndex = -1;
        for (int i = intervals.size()-1; i >= 0; --i) 
        {
            if (intervals[i].start > covered)
                continue; 
            if (intervals[i].end < covered) // no intervals can cover
                return std::vector<int>();

            foundIndex = i;
            covered = intervals[i].end;
            break;
        }

        if (foundIndex != -1) 
        {
            cover.push_back(intervals[foundIndex].index);
            intervals.erase(intervals.begin()+foundIndex);
        }
        else
        {
            return std::vector<int>();
        }

        if (zeroInterval) // 0-intervals only need one pass
            break;
    }
    if (covered < interval.end) 
        return std::vector<int>();
    
    return cover;
}

}//namespace minium