///////////////////////////////////////////////////////////////////////////////////
/// Minium Algorithm Library.
///
///		Fenwick Tree (prefix sums) 
///
///////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <math.h>

namespace minium 
{

/*
 * @brief: i mod m
 * @param: i: 
 * @param: m: 
 * @return: i mod m
 */
template<typename T>
T mod(T i, T m) 
{
    if (i%m < 0)
        return (i%m + m);
    return (i%m);
}

/*
 * @brief: Calculates the Greatest common Divisor of a and b
 * @param: a: 
 * @param: b: 
 * @return: the greatest common divisor of a and b
 */
template<typename T>
T gcd(T a, T b)
{
	T c;
	while (a != 0)
	{
		c = a;
		a = b % a;
		b = c;
	}
	return b;
}

/*
 * @brief: Calculates Least Common Multiple
 * @param: a: 
 * @param: b: 
 * @return: the least common multiple of a and b
 *
 * TODO: precision return type
 */
template<typename T>
T lcm(T a, T b)
{
	return a * b / gcd(a, b);
}

/*
 * @brief: Integer division
 * @param: a: the numerator
 * @param: b: the denominator
 * @return: a/b
 */
template<typename T>
T int_div(T a, T b) 
{
    if (a >= 0)
        return (T)floor(a/b); // superfluous
    return (T)ceil((double)a/b);
}

/*
 * @brief: the Extended Eucledian Algorithm finds x,y such that x*a + y*b = gcd(a,b)
 * @source: http://en.wikipedia.org/wiki/Extended_Euclidean_algorithm
 * @return: (x,y)
 */
template<typename T>
std::pair<T,T> euclidean(T a, T b) 
{
    if (a == 0) return std::make_pair(0,1);
    else if (b == 0) return std::make_pair(1,0);

    std::pair<T,T> yx = euclidean(b, mod(a,b));
    T x = yx.second;
    T y = yx.first;
    return std::make_pair(x, y - int_div(a,b)*x);
}

/*
 * @brief: adds two numbers a and b that are congruent modulo m
 * @return: (a+b) mod m
 */
template<typename T>
T mod_add(T a, T b, T m) 
{
    T c = a + b;
    if (c >= m)
        c = c - m;
    return c;
}

/*
 * @brief: subtracts two numbers a and b that are congruent modulo m
 * @return: (a-b) mod m
 */
template<typename T>
T mod_sub(T a, T b, T m)
{
    T c;
    if (b > a)
        c = m + a - b;
    else 
        c = a - b;
    return c;
}


/*
 * @brief: 	Multiples modulo m using iterative addition that makes use of the fact that:
 * 			x Â· y = x Â· (y mod 2) + x Â· (2 Â· âŒŠ y âŒ‹)
 *
 * @return: a*b mod m
 *
 * TODO: remove static_cast
 */
template<typename T>
T mod_mult(T a, T b, T m)
{
    T c = 0;
    while (b != 0) {
        if (mod(b,static_cast<T>(2)) != 0) {
            c = mod_add(c,a,m);
        }
        a = mod_add(a,a,m);
        b = (T)floor((double)b/2); // superfluous
    }
    return c;
}
 
/*
 * @brief: 	Divides modulo m, which is defined as the inverse of multiplication:
 * 			a/b mod m = a*(b^-1) mod m, where the inverse is found using the euclidean algorithm for GCD.
 * 			If no inverse exists, -1 is returned.
 * 
 * @return: a/b mod m
 */
template<typename T>
T mod_div(T a, T b, T m) {
    std::pair<T, T> xy = euclidean(b,m); // find b^-1
    T x = xy.first;
    T y = xy.second;
    if ((x*b + y*m) != 1) {
        return -1;
    }
    x = mod(x,m);
    return mod_mult(a,x,m);
}



/*
 * @brief: calculates (a^b) mod c
 */
template<typename T>
T mod_pow(T a, T b, T c)
{
    T res = 1;
    for (int i = 0; i < b; ++i)
        res = (res * a) % c;
    return res % c;
}

/* @brief: 	Multiplicative Inverse Modular
 *			a^(phi(m)) = 1 (mod m)
 * 			a^(-1) = a^(m-2) (mod m) 
 *
 * TODO: template arguments
 */
long long InverseEuler(int n, int m)
{
	return mod_pow(n, m-2, m);
}

}//namespace minium
