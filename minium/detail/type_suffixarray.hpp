///////////////////////////////////////////////////////////////////////////////////
/// Minium Algorithm Library.
///
/// 	Suffix Array
///
/// Data structure for suffix arrays and longest common prefix (LCP) arrays
///
///////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>

namespace minium 
{

class SuffixArray
{
public:

	/*
	 *
	 */
	SuffixArray(std::string s);
	~SuffixArray();

	/*
	 *
	 */
	int getSuffix(int i);

	/*
	 *
	 */
    int getLCP(int i);


protected:
private:
	const static int MAXN = 100001;
	std::string S;
	int N, gap;

	int sa[MAXN], lcp[MAXN];
	int r_sa[MAXN], r_lcp[MAXN];

    /*
     * Constructs a longest common prefix (LCP) array using the suffix array
     */
	void buildLCP();
};

}//namespace minium

#ifndef MINIUM_EXTERNAL_TEMPLATE
#include "type_suffixarray.inl"
#endif//MINIUM_EXTERNAL_TEMPLATE