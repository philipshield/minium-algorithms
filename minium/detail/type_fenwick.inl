///////////////////////////////////////////////////////////////////////////////////
/// Minium Algorithm Library.
///
/// 	Fenwick Tree implementations
///
///////////////////////////////////////////////////////////////////////////////////


namespace minium
{

/////////////////////////////////
// constructors

template<typename T>
FenwickTree<T>::FenwickTree(int N)
	: N(N+1), tree(N+1,0) 
{
}

template<typename T>
FenwickTree<T>::FenwickTree(FenwickTree& other) 
	: N(other.N), tree(other.tree)
{
}

template<typename T>
FenwickTree<T>::FenwickTree(FenwickTree&& other) 
	: N(std::move(other.N)), tree(std::move(other.tree))
{
}

template<typename T>
FenwickTree<T>::~FenwickTree() 
{
}

/////////////////////////////////
// public methods
template<typename T>
void FenwickTree<T>::update(int idx, T val)
{
    if (idx == 0) 
    {
        tree[0] += val;
    }
    else 
    {
        while (idx <= N) 
        {
            tree[idx] += val;
            idx += idx&(-idx);
        }
    }
}

template<typename T>
T FenwickTree<T>::query(int idx) 
{
	--idx; 			// queries do not include tree[idx] (exclusive)
    if (idx == -1) 	// special case for querying tree[0], which should always return 0
        return 0;

    T sum = tree[0];
    while (idx > 0) 
    {
        sum += tree[idx];
        idx -= idx&(-idx);
    }
    return sum;
}


} //namespace minium