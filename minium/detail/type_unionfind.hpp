///////////////////////////////////////////////////////////////////////////////////
/// Minium Algorithm Library.
///
///	    Union Find
///
///////////////////////////////////////////////////////////////////////////////////

#pragma once

namespace minium 
{

/*
 * @brief: A data structure that handles union-find operations for sets
 */
class UnionFind
{
public:

    /*
     * @brief: Constructor. The initial sets are {1}, {2},..., {N}.
     */
    UnionFind(int N);
    ~UnionFind();

    /*
     * @brief: Unite the sets that contain parameters p and q
     */
    void merge(int p, int q);

    /*
     * @brief: returns whether p and q are in the same set
     */
    bool same(int p, int q);

    /*
     * @brief: returns the (representative of the) set that contains p
     */
    int find(int p);
    
protected:
private:
    int N;
    int* parent;
    int* height;

};

}//namespace minium


#ifndef MINIUM_EXTERNAL_TEMPLATE
#include "type_unionfind.inl"
#endif//MINIUM_EXTERNAL_TEMPLATE