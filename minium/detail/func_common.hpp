///////////////////////////////////////////////////////////////////////////////////
/// Minium Algorithm Library - Datastructures
///
/// 	Common Header
///
///	Includes other common headers such as setup.
/// Includes common functions such as sgn(x) etc.
/// 
///////////////////////////////////////////////////////////////////////////////////


#pragma once

namespace minium 
{

template<typename T> 
sgn(T x)
{
	return (x > 0) ? 1 : ((x < 0) ? -1 : 0);
}


}//namespace minium