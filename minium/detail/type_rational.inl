///////////////////////////////////////////////////////////////////////////////////
/// Minium Algorithm Library.
///
///		Rational Number Class implementation
///
///////////////////////////////////////////////////////////////////////////////////

namespace minium
{

	/////////////////////
	// constructors

	template<typename T>
	Rational<T>::Rational(T numerator, T denominator, bool doreduce)
		: numerator(numerator), denominator(denominator), reduced(false)
	{
		if (doreduce) reduce();
	}

	template<typename T>
	Rational<T>::Rational(T numerator)
		: numerator(numerator), denominator(1), reduced(true)
	{
	}

	template<typename T>
	Rational<T>::Rational()
		: numerator(0), denominator(0), reduced(true)
	{
	}


	/////////////////////
	// public methods

	template<typename T>
	Rational<T> Rational<T>::inverse() const
	{
		return Rational<T>(denominator, numerator);
	}


	/////////////////////
	// operators

	template<typename T>
	Rational<T>& Rational<T>::operator=(const Rational<T>& other)
	{
		numerator = other.numerator;
		denominator = other.denominator;
		reduced = other.reduced;
		return *this;
	}

	template<typename T>
	Rational<T> Rational<T>::operator*(const Rational<T>& other) const
	{
		return Rational(
			numerator*other.numerator,
			denominator*other.denominator); //reduced in constructor
	}

	template<typename T>
	Rational<T> Rational<T>::operator+(const Rational<T>& other) const
	{
		Rational a(numerator*other.denominator, denominator*other.denominator, false);
		Rational b(other.numerator*denominator, other.denominator*denominator, false);
		return Rational(a.numerator + b.numerator, a.denominator);
	}

	template<typename T>
	Rational<T> Rational<T>::operator-(const Rational<T>& other) const
	{
		Rational a(numerator*other.denominator, denominator*other.denominator, false);
		Rational b(other.numerator*denominator, other.denominator*denominator, false);
		return Rational(a.numerator - b.numerator, a.denominator);
	}

	template<typename T>
	Rational<T> Rational<T>::operator/(const Rational<T>& other) const
	{
		// (x/y)/(a/b) = (x/y)*(b/a)
		return (*this) * other.inverse();
	}

	template<typename T>
	const bool Rational<T>::operator==(const Rational<T>& other) const
	{
		if (reduced && other.reduced) //often the case
			return numerator == other.numerator && denominator == other.denominator;

		return Rational(*this) == Rational(other); //reduce and compare
	}

	template<typename T>
	const bool Rational<T>::operator!=(const Rational<T>& other) const
	{
		return !(*this == other);
	}

	template<typename T>
	const bool Rational<T>::operator<=(const Rational<T>& other) const
	{
		return numerator*other.denominator <= other.numerator*denominator;
	}

	template<typename T>
	const bool Rational<T>::operator>=(const Rational<T>& other) const
	{
		numerator*other.denominator >= other.numerator*denominator;
	}

	template<typename T>
	const bool Rational<T>::operator<(const Rational<T>& other) const
	{
		return numerator*other.denominator < other.numerator*denominator;
	}

	template<typename T>
	const bool Rational<T>::operator>(const Rational<T>& other) const
	{
		return numerator*other.denominator > other.numerator*denominator;
	}

	/////////////////////
	// ostream

	template<typename T>
	std::ostream& operator<< (std::ostream& out, const Rational<T>& r)
	{
		if (!SHOW_DIV_BY_1 && r.denominator == 1)
			return out << r.numerator;
		else
			return out << r.numerator << " / " << r.denominator;    
	}

}//namespace minium