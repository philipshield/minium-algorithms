///////////////////////////////////////////////////////////////////////////////////
/// Minium Algorithm Library.
///
///		Fenwick Tree (prefix sums) 
///
///////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <vector>

namespace minium 
{

/*
 * @brief: Data structure for calculating and updating prefix sums.
 * @update: Increments index idx by val, including related prefix sums
 * @query: Returns the prefix sum for indices 0 to idx-1. index 0 always returns 0.
 */
template<typename T>
class FenwickTree
{
public:
	FenwickTree(int N);
	FenwickTree(FenwickTree& other);
	FenwickTree(FenwickTree&& other);
	~FenwickTree();

    /*
     * @brief:Updates the prefix sums
     * @param: idx: index to update from
     * @param: val: value to increment by
     */
    void update(int idx, T val);

    /*
     * brief: Query the tree for prefix sums (exclusive)
     *
     * @param: idx: index from which to sum prefix sums (exclusive)
     * @return: tree[0] + tree[1] + ... + tree[idx-1]
     */
	T query(int idx);

protected:
private:
	std::vector<T> tree;
	int N;
};

}//namespace minium


#ifndef MINIUM_EXTERNAL_TEMPLATE
#include "type_fenwick.inl"
#endif//MINIUM_EXTERNAL_TEMPLATE