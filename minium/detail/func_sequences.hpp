///////////////////////////////////////////////////////////////////////////////////
/// Minium Algorithm Library.
///
///		Sequence Algorithms
///
///////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <vector>

namespace minium 
{


/*
 * @brief: Finds the longest increasing subsequence in an array of integers 
 * @param objects: a vector of integers
 * @return: the indices in the vector for one longest increasing subsequence
 */
std::vector<int> lis(std::vector<int> objects);

/* 
 * @brief: Interval struct used in interval covering
 * @param: index: The original index of the interval in the list of intervals to use
 */
struct Interval {
    int index;
    double start;
    double end;
    Interval() : index(-1), start(0), end(0) {};
    Interval(double start, double end) : start(start), end(end) {};
    Interval(const Interval& other) : index(other.index), start(other.start), end(other.end) {};
    bool operator < (const Interval& i) const { return (end < i.end); } // compare intervals by end
};

/* 
 * @brief: Finds the minimum number of intervals that can be used to cover an interval
 *
 * @param interval: the interval to cover
 * @param intervals: the intervals to cover with
 * @return: a vector containing the indices of the intervals used to cover the interval,
            or an empty vector if no covering is found
 */
std::vector<int> cover(Interval interval, std::vector<Interval> intervals);


}//namespace minium


#ifndef MINIUM_EXTERNAL_TEMPLATE
#include "func_sequences.inl"
#endif//MINIUM_EXTERNAL_TEMPLATE