///////////////////////////////////////////////////////////////////////////////////
/// Minium Algorithm Library. Datastructures.
///
/// 	Graph Implementation
///
///////////////////////////////////////////////////////////////////////////////////

#include <cassert>

#define rep(i, a, b) for (int i = (a); i < (b); ++i)                         // for loop
#define urep(i, a, b) for (size_t i = (a); i < (b); ++i)					 // unsigned for loop
#define trav(it, v) for (auto it = (v).begin(); it != (v).end(); ++it)       // foreach loop

namespace minium
{

/////////////////////////////////
// constructors
Node::Node(int id) 
	: id(id)
{ }

Node::Node() 
	: id(-1) 
{}

Graph::Graph()
	: N(0)
{ }

Graph::Graph(int N)
	: N(N)
{
	V.resize(N);
	rep(i, 0, N) V[i].id = i;
}

Graph::~Graph()
{ }


/////////////////////////////////
// public methods

void Node::add(int from, int to, double w)
{
	adj.emplace_back(Edge(from, to, w));
}

void Node::add(Edge e)
{
	adj.push_back(e);
}

const Node& Graph::getNode(int idx) const
{
	assert(idx < N);
	return V[idx];
}

const std::vector<Edge>& Graph::getAdj(int idx) const
{
	return getNode(idx).adj;
}

void Graph::addEdge(int from, int to, double w)
{
	Edge edge(from, to, w);
	V[from].add(edge);
	E.push_back(edge);
	M++;	
}

int Graph::getWeight(int from, int to)
{
	trav(e, getNode(from).adj)
	{
		if ((*e).to == to)
			return (*e).w;
	}

	return -1;
}

int Graph::getPathLength(std::vector<int> path)
{
	int res = 0;

	if (path.size() >= 2)
	{
		urep(i, 0, path.size() - 1)
		{
			res += getWeight(path[i], path[i + 1]);
		}
	}

	return res;
}

} //namespace minium

#undef rep
#undef urep
#undef trav