///////////////////////////////////////////////////////////////////////////////////
/// Minium Algorithm Library.
///
/// 	Fenwick Tree implementations
///
///////////////////////////////////////////////////////////////////////////////////

namespace minium
{

/////////////////////////////////
// constructors

UnionFind::UnionFind(int n)
: N(n + 1), parent(new int[n + 1]), height(new int[n + 1])
{
    for(int i = 0; i < n+1; ++i)
    {
        parent[i] = i;
        height[i] = 0;
    }
}

UnionFind::~UnionFind()
{
    delete[] parent;
    delete[] height;
}

/////////////////////////////////
// public methods

void UnionFind::merge(int p, int q)
{
    int A = find(p);
    int B = find(q);
    if (A == B) return; //already in same set

    if (height[A] < height[B])
        parent[A] = B;
    else if (height[A] > height[B])
        parent[B] = A;
    else
    {
        parent[B] = A;
        height[B]++;
    }
}

bool UnionFind::same(int p, int q)
{
    int id_p = find(p);
    int id_q = find(q);

    return id_p == id_q;
}


/////////////////////////////////
// private methods

int UnionFind::find(int p)
{
    if (parent[p] == p) return p;

    int res = find(parent[p]); //TODO: path compression
    parent[p] = res;
    return res;

}



} //namespace minium