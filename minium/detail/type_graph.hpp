///////////////////////////////////////////////////////////////////////////////////
/// Minium Algorithm Library. Datastructures.
///
///		Graph Datastructure
///
///////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <vector>

namespace minium 
{

//forward declerations
class Node;
class Edge;
class Graph;


/*
 * brief: 
 */
class Edge
{
public:
    int from;
    int to;
    double w;

    Edge(int from, int to, double w) : from(from), to(to), w(w) {}

protected:
private:
};



/*
 * brief: 
 */
class Node
{
public:
    int id;
    std::vector<Edge> adj;

    Node(int id);
    Node();

    void add(int from, int to, double w);
    void add(Edge e);

protected:
private:
};


/*
 * brief: 
 */
class Graph
{
public:
    int N;
    int M;
    std::vector<Node> V;
    std::vector<Edge> E;

    Graph();
    Graph(int N);
    ~Graph();

    const std::vector<Edge>& getAdj(int idx) const;
    const Node& getNode(int idx) const;
    int getPathLength(std::vector<int> path);
    int getWeight(int from, int to);

    void addEdge(int from, int to, double w = 0);

protected:
private:
};


}//namespace minium


#ifndef MINIUM_EXTERNAL_TEMPLATE
#include "type_Graph.inl"
#endif//MINIUM_EXTERNAL_TEMPLATE