///////////////////////////////////////////////////////////////////////////////////
/// Minium Algorithm Library - Algorithms
///
///		Graph Algorithms
///
///////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <vector>

namespace minium 
{


/*
 * @brief: Constructs a Minimum Spanning Tree (MST) of a graph G.
 * @return: A minimum spanning tree of G as a vector of edges.
 * If multiple MST's exist, an arbitrary one is returned.
 * If forests exist, an empty vector is returned.
 */
std::vector<Edge> MinimumSpanningTree(Graph G);


}//namespace minium


#ifndef MINIUM_EXTERNAL_TEMPLATE
#include "func_graph.inl"
#endif//MINIUM_EXTERNAL_TEMPLATE