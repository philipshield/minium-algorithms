///////////////////////////////////////////////////////////////////////////////////
/// Minium Algorithm Library - Algorithms
///
/// 	Graph Implementation
///
///////////////////////////////////////////////////////////////////////////////////

#include "type_unionfind.hpp"

#define rep(i, a, b) for (int i = (a); i < (b); ++i)                         // for loop
#define trav(it, v) for (auto it = (v).begin(); it != (v).end(); ++it)       // foreach loop

namespace minium
{

std::vector<Edge> MinimumSpanningTree(Graph G)
{
	std::vector<Edge> mst;
	UnionFind U(G.N);
	std::vector<Edge> edges = G.E;
	sort(edges.begin(), edges.end(), [](Edge a, Edge b){ return a.w < b.w; });

	trav(e, edges)
	{
		int from = (*e).from;
		int to = (*e).to;
		if (!U.same(from, to))
		{
			int w = (*e).w;
			if (from < to)
				mst.push_back(Edge(from,to,w));
			else 
				mst.push_back(Edge(to,from,w));
			U.merge(from, to);
		}
	}

	bool isforest = false;
	int s = U.find(0);
	rep(i, 1, G.N)
	{
		if (U.find(i) != s)
		{
			isforest = true;
			break;
		}
	}

	if (!isforest)
	{
		return mst;
	}
	else
	{
		return std::vector<Edge>();
	}
}


} //namespace minium

#undef rep
#undef trav