///////////////////////////////////////////////////////////////////////////////////
/// Minium Algorithm Library - Datastructures
///
/// 	Fenwick Tree.
///
///	The fenwick tree is a binary indexed tree which manipulates cumulative 
/// frequency tables.
/// 
///////////////////////////////////////////////////////////////////////////////////


#pragma once

#include "detail/type_fenwick.hpp"

namespace minium 
{

}//namespace minium