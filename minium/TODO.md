
## Bugs and fixes ##

* minium typedefs
	- precision
	- size_t
	- etc


* BUG in mod_div
* .inl for func_xyz (modular.inl for example) =>
* undefine common.hpp macros =>
* generate documentation =>

* style guide
	- documentation
	- functions
	- classes
	- namespaces

* checkout External .inl templates => CHECK




## The Road Ahead ##

### Datastructures ###

* Rational Numbers =>
* Fenwick Tree => CHECK

### Algorithms ###

* modular arithmetic =>





