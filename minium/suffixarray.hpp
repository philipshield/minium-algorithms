///////////////////////////////////////////////////////////////////////////////////
/// Minium Algorithm Library - Datastructures
///
/// 	Suffix Array
///
///	A sorted array of sorted suffixes of a string with operations for Longest Common Prefix
/// 
/// 
///////////////////////////////////////////////////////////////////////////////////


#pragma once

#include "detail/type_suffixarray.hpp"

namespace minium 
{

}//namespace minium