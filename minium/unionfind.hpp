///////////////////////////////////////////////////////////////////////////////////
/// Minium Algorithm Library - Datastructures
///
/// 	Union Find
///
/// A disjoint-set data structure. Keeps track of a set of elements partitioned into
/// disjoints set. It has operations for joining sets, moving elements between sets 
/// and querying to which set an element belongs.
/// 
///////////////////////////////////////////////////////////////////////////////////


#pragma once

#include "detail/type_unionfind.hpp"

namespace minium 
{

}//namespace minium