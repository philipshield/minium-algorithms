///////////////////////////////////////////////////////////////////////////////////
/// Minium Algorithm Library - Algorithms
///
/// 	Sequence Algorithms
///
///	Algorithms related to sequences, such as Longest Increasing Subsequence or string algorithms
/// 
///////////////////////////////////////////////////////////////////////////////////


#pragma once

#include "detail/func_sequences.hpp"

namespace minium 
{

}//namespace minium