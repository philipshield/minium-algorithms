# Minium Algorithms#
**Minium** is a header-only library of common algorithms and datastructures.

The *minium mineral* is the naturally occurring form of lead tetroxide, Pb2+2Pb4+O4, also known as red lead.

![alt text] (https://bitbucket.org/philipshield/minium-algorithms/raw/master/images/minium-wikipedia.jpg "the minium mineral")

# Data Structures #
* **Fenwick Tree** - calculating prefix sums
* **Rational** - representing rational numbers
* **Suffix Array** - suffix and longest common prefix (LCP) arrays
* **Union Find** - disjoint-set data structure
* **Graph** - a simple graph class

# Algorithms #
### Sequence algorithms ###
Provides common algorithms for sequences.

* **LIS** - Longest Increasing Subsequence
* **Interval Cover** - Finds the minimum number of intervals that can be used to cover an interval

### Graph Algorithms###
Provides common graph algorithms.

* **MST** - Minimum Spanning Tree

### Modular arithmetic ###
Provides common functions for modular arithmetic.

* **mod** - *i* mod *m*
* **gcd** - greatest common divison
* **lcm** - least common multiple
* **int div** - integer division
* **euclidean** - Euclid's extended algorithm
* **mod add/sub/div/mult/pow** - basic operations, congruent modulo m
* **inverse euler** - modular multiplicative inverse